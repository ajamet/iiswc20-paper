\section{Results, analysis and discussion}
\label{sec:results_discussion}

This section presents our experimental campaign along with the results
obtained and the characterization of the studied workloads.
Section \ref{subsec:performance} presents the performance benefits yielded by
the different state of the art cache replacement policies mentioned in
Sections \ref{subsec:replacement_policies_simulated} and
\ref{sec:design_proposal}. Section \ref{subsec:misses} studies the
impact of these workloads on the LLC in terms of misses and presents the
MPKI reduction obtained by the replacement policies mentioned above.
Finally, Section \ref{subsec:reuse_distances} studies the behavior
of the studied workloads via the reuse distance of the cache blocks to
highlight the different behavior of these benchmark suites.

\begin{figure*}[h]
  \centering
  \subfloat[LLC MPKI using the LRU replacement policy for the 50 most intensive workloads.]{
    \includegraphics[width=\textwidth]{results/plots/intensives_mpki.pdf}
    \label{fig:intensives_mpki}
  }

  \vspace{-0.25cm}

  \subfloat[Speed-up over LRU of the state-of-the-art LLC replacement policies and our custom replacement policies for the 50 most intensive workloads.]{
    \includegraphics[width=\textwidth]{results/plots/intensives_speedup.pdf}
    \label{fig:intensives_speedup}
  }
  \caption{LLC MPKI using the LRU replacement policy and performance improvement of LLC replacement policies for the 50 most intensive workloads.}
  \vspace{-0.5cm}
  \label{fig:intensives_results_speedup_mpki}
\end{figure*}

\subsection{Misses}
\label{subsec:misses}

Figure \ref{fig:intensives_mpki} shows the LLC MPKI of the 50 most intensive
workloads using the LRU replacement polity
%, against various techniques presented in
%\ref{subsec:replacement_policies_simulated} and in
%\ref{sec:design_proposal}, among all the workloads available.
These workloads show a very high
impact on the LLC, with an average MPKI of 120,
much larger to the workloads used in previous work.
We use this to define the set of workloads that will be
used in Section~\ref{subsec:performance}
%and more precisely in figure \ref{fig:intensives_speedup}
to evaluate state-of-the-art
replacement policies along with our custom designs.
These results clearly show the high impact on the LLC of Qualcomm, GAP and XSBench workloads.
This selection of benchmarks is comprised of each of the studied benchmark suites and
allows to evaluate replacement policies against a broad range of workloads with different behaviors.

% \begin{figure*}[!h]
%   \includegraphics[width=\textwidth]{results/plots/intensives_mpki.pdf}
% 	\vspace{-0.5cm}
%   \caption{LLC MPKI of the 50 most intensive workloads using the baseline LRU policy.}
%   \vspace{-0.5cm}
%   \label{fig:llc_mpki_intensive_workloads}
% \end{figure*}

\subsection{Performance}
\label{subsec:performance}

% \begin{figure*}[h!]
%   \includegraphics[width=\textwidth]{results/plots/intensives_speedup.pdf}
% 	\vspace{-0.5cm}
%   \caption{Speed-up results of the LLC replacement policies on the 50 most intensive workloads}
%   \vspace{-0.5cm}
%   \label{fig:speedup_intensive_workloads}
% \end{figure*}

% Thinking about moving this in the motivation section.

Figure \ref{fig:intensives_speedup} shows the
speed-up of various replacement policies presented in
Section \ref{subsec:replacement_policies_simulated} and in
Section \ref{sec:design_proposal}. LLC MPKI sorts the benchmarks with a
baseline LRU policy. While Figure \ref{fig:speedup_suites_split} was
showing Glider standing out against MPPPB in some situations, Figure
\ref{fig:intensives_speedup} clearly shows the versatility of
the former and its ability to consistently deliver good performance even
facing the most intensive and hard to predict workloads. MPPPB and
Glider provide respectively \SI{7.0}{\percent} and \SI{5.0}{\percent} speed-up over baseline
LRU.
Thus, Glider is not able to deliver significant improvement over the baseline LRU compared with MPPPB. This
fact suggests that the machine learning algorithm used to design the predictor of Glider should be trained against a wider variety
of workloads, hence the need to include the workloads used in this paper in further work on LLC replacement policies.

% \todo{Explain the reason thath justifies the differnece of results between Hawkeye and Multiperspective.}

Besides, we justify the difference of performance improvement in Figures
\ref{fig:speedup_suites_split} and \ref{fig:intensives_results_speedup_mpki} between Glider and MPPPB compared to the
results published in \textit{Applying Deep Learning to the Cache Replacement Problem}\cite{shi_applying_2019} by a difference
in the methodology. As a matter of fact, we use the SimPoint methodology to generate at most ten simpoints and we only
use the ones accounting for more than \SI{5}{\percent} of the whole execution, whereas the original results of Glider were reported
with only a single trace per benchmark. We argue that our methodology is more robust as we cover more
distinct behaviors, thus challenging the different techniques studied.

We also report significant performance improvements for our proposed techniques derived from MPPPB.
The two designs proposed in this paper, MS-MPPPB and DS-MPPPB, respectively yield
\SI{8.3}{\percent} and \SI{8.0}{\percent} speed-up over the baseline LRU,
and both new approaches also outperform MPPPB.
We observe that this performance improvement is due to the ability to adapt
the set of features used for prediction in a execution phase-wise manner, providing
more versatility to the design than the previously discussed techniques.

\subsection{Reuse distances}
\label{subsec:reuse_distances}

Figure \ref{fig:reuse_distances_distribution} shows the
distribution of reuse distances for each benchmark suite used
in this work.
Figure \ref{fig:reuse_distances_distribution}
is organized as a box plot; the horizontal line of each distribution representing its median, the box ranging
from the first to the third quartiles and the whiskers representing extreme values through the first and ninth deciles
of the distributions.
For readability purposes, we cut the box plot to only show whiskers but not flyers.
For instance, the distribution of reuse distances of the GAP workloads have ninth decile around \num{2000}
and a median of \num{116}.
We complete figure
\ref{fig:reuse_distances_distribution} with Table
\ref{tab:reuse_distance_distribution_parameters} as it provides
additional characteristics of the distribution such as maximum, median,
mean and standard deviation of the observed reuse distances.

\input{results/reuse_distance_distributions_parameters.tex}

In this work, we define reuse distance as
the number of accesses to different cache blocks between two consecutive accesses coming from the
CPU (\emph{e.g.} read and writes but not prefetches and write-backs) to
the same cache block. Looking at this figure, it is quite clear that
all the benchmark suites suffer from the presence of dead blocks
in their access patterns. However, having a closer look at each of the
distribution presented can provide us with valuable knowledge and give
intuition about the results obtained in Sections
\ref{subsec:performance} and \ref{subsec:misses}. Blocks that do not experience a single reuse during the whole execution
of a workload are not represented on Figure \ref{fig:reuse_distances_distribution}. However, in order to still provide
that valuable information, we show in Table \ref{tab:reuse_distance_distribution_parameters} the average proportion
of accesses to dead blocks during the execution.

% \begin{figure}[h!]
%   \includegraphics[width=\columnwidth]{results/plots/reuse_distance_density_suites_split.pdf}
% 	\vspace{-0.5cm}
%   \caption{Distribution of the reuse distances of the cache blocks per benchmark suite.}
%   \vspace{-0.2cm}
% 	% \vspace{-0.4cm}
%   \label{fig:reuse_distances_distribution_suites_split}
% \end{figure}

For the rest of this section, we define as \emph{cache-friendly blocks}
those blocks with a reuse distance lower than the associativity of the LLC.
Conversely, we define \emph{cache-averse blocks} as blocks with a reuse distance
higher than the LLC associativity. Assuming an LRU policy for the cache,
if a cache block A gets inserted in the LLC and the cache sees 16
accesses to different blocks before re-referencing block A,
the second access to block A will cause a miss because it will have been evicted.
This block A occupies part of the cache capacity that can be used to allocate another block,
hopefully cache-friendly, and can provide a hit instead of miss.

\begin{figure}[h]
  \centering


  \subfloat[Distribution of the reuse distances of the cache blocks for the different benchmark suites.]{
    \includegraphics[width=.95\columnwidth]{results/plots/reuse_distance_density_suites_split.pdf}
    \label{fig:reuse_distances_distribution_suites_split}
  }

  \vspace{-0.25cm}

  \subfloat[Distribution of the reuse distances of the cache blocks for the GAP benchmark suite with different input sets.]{
    \includegraphics[width=.95\columnwidth]{results/plots/reuse_distance_density_gapbs_split.pdf}
    \label{fig:reuse_distances_distribution_gapbs_split}
  }
  \caption{Distribution of the reuse distances of the cache blocks for the different benchmark suites.}
  \vspace{-0.5cm}
  \label{fig:reuse_distances_distribution}
\end{figure}

% Maybe a figure to explain the idea of reuse distance in a more graphical way. This figure could coupled with a short explaination.

\subsubsection{SPEC benchmarks}
\label{subsubsec:spec_benchmarks}

% With an average reuse distance of \num{126} and a standard eviation as low as \num{572.94}, SPEC CPU 2006 \& SPEC CPU 2017

As Figure \ref{fig:reuse_distances_distribution_suites_split} and Table \ref{tab:reuse_distance_distribution_parameters} show,
the SPEC workloads present an average reuse distance of \num{126} and a standard deviation as low as \num{572.94},
%, \textit{SPEC} CPU 2006 \& \textit{SPEC} CPU 2017,
so the aggregated distribution of reuse distances for these workloads
focuses mainly on low values with a relatively low standard deviation
which translates into a cache-friendly behavior. Also, most
accesses being cache-friendly, a bypass policy is not required to
achieve reasonable performance over LRU.

This behavior explains why policies such as Hawkeye and Glider
perform well when applied to SPEC workloads. These
policies are focusing on prioritizing eviction for blocks that
previously showed cache-averse behavior. Doing so allows to free space
for cache-friendly blocks, however, as we will see in details for GAP
benchmarks and XSBench benchmarks in
the next subsections,
%Sections \ref{subsubsec:gapbs_benchamrks_reuse_distances} and
%\ref{subsubsec:xsbench_benchmarks_reuse_distances},
replacement policies in general are less effective when the memory
footprint increases and the access patterns become unpredictable.

\subsubsection{XSBench benchmarks}
\label{subsubsec:xsbench_benchmarks_reuse_distances}

The \emph{XSBench} benchmark suite, as expected based on the reuse distance characteristics established in Table
\ref{tab:reuse_distance_distribution_parameters}, shows a very
distinct behavior from the well-known \emph{SPEC} CPU 2006 and \emph{SPEC} CPU 2017 workloads.
As a matter of fact, the \textit{XSBench} workloads present an average reuse distance of \num{343.23}
and a standard deviation as high as \num{875.68}, which reveals a hard to predict behavior. However,
they do experience less dead accesses to the LLC, the proportion of dead block accesses being \SI{37.32}{\percent}.

With both mean and median reuse distances being the double of the
\emph{SPEC} ones, it is clear that these workloads are
much more biased towards a cache-averse behavior than the SPEC workloads. However, with such
high distribution parameters, we can still still make a crucial observation when it
comes to reuse prediction and dead blocks. We observe that, even though
the \emph{XSBench} workloads experience reasonable reuse of cache blocks
that are accessed more than once during the execution,
this happens for all the grid types except the \emph{unionized},
%only for a small fraction of the cache blocks,
%so the LLC is suffering from poor
%reuse of cache blocks. This behavior is particularly dominant in the
%workloads using the \emph{unionized} grid type,
which has a substantially larger memory footprint than the other grid types.
We explain such behavior by the vast amount of data that the solver
needs to traverse during the workloads execution and the
algorithms used to do so.

% Here, we might need to go in details in the algorithms used and the topology of data.

\subsubsection{Qualcomm benchmarks}

When it comes to Qualcomm workloads, we observe that
the distribution of reuse distances stretches towards higher values (Table \ref{tab:reuse_distance_distribution_parameters} shows that the
tail of the distribution goes as high as \num{60000} accesses). However, these workloads are relatively biased towards a cache-friendly behavior as the standard deviation is \num{541.80} and the dead block accesses proportion is of \SI{10.54}{\percent}.
This stretched shape leads to a rather low standard deviation and to the appearance of low reuse distances with higher probability than of high reuses distances. We
understand the long-wide tail of the distribution as a low
amount of dead blocks thrashing the LLC, using space
that would be a better fit for cache-friendly blocks.

Thus, this observation gives us intuition about the behavior of Glider
on the Qualcomm benchmarks presented in Figures
\ref{fig:intensives_results_speedup_mpki} and \ref{fig:speedup_suites_split}. Glider is a replacement
policy that does not implement bypass, so it keeps on inserting
dead blocks in the cache even though it has learned that these blocks
are cache-averse. The absence of a bypass policy explains the poor results
of Glider compared to MPPPB and the two proposed approaches derived from it.

\subsubsection{GAP benchmarks}
\label{subsubsec:gapbs_benchamrks_reuse_distances}

% \begin{figure}[h!]
%   \includegraphics[width=\columnwidth]{results/plots/reuse_distance_density_gapbs_split.pdf}
% 	\vspace{-0.5cm}
%   \caption{Distribution of the reuse distances of the cache blocks for GAP workloads separated between \texttt{krand \& urand} inputs and others.}
% 	\vspace{-0.5cm}
%   \label{fig:reuse_distances_distribution_gapbs_split}
% \end{figure}

Based on the characteristics of the reuse distance distribution shown in Table \ref{tab:reuse_distance_distribution_parameters}
and Figure \ref{fig:reuse_distances_distribution} for the
GAP benchmark suite, these graph processing workloads
show a very cache-averse behavior with an average reuse distance of \num{907.89},
which is around 5.8 times higher than the one of SPEC workloads,
and a proportion of dead block accesses of \SI{55.84}{\percent}. Such behavior is expected
as GAP workloads are executing graph processing algorithms and are known to
traverse vast amount of data in a very unpredictable way.
% Highly speculative. Needs to be checked. For some benchmarks that shows no reuse e.g. urand inputs even infinite caches
% would not be of any help.
Table \ref{tab:reuse_distance_distribution_parameters} shows a median of 28 accesses,
which provides us with useful information as \SI{50}{\percent} of
the blocks accessed experience a reuse distance of 28 or fewer accesses.
This characteristic suggests that having higher LLC associativity could help to achieve higher performance.

In addition, these benchmarks show very high standard
deviation on reuse distances, which suggests that the CPU triggers
accesses to both cache-averse and cache-friendly blocks with relatively uniform probabilities,
which results in the
eviction of useful blocks. Thus, we deduce that using a bypass policy
like MPPPB does would provide performance benefits. Results shown for GAP
benchmarks on Figure \ref{fig:intensives_results_speedup_mpki} highlight
this property of graph processing workloads.

As we observe that GAP benchmarks with inputs \texttt{kron} and
\texttt{urand} show dramatically low reuse of cache blocks during
execution, we categorize GAP benchmarks in two categories:
(i) GAP workloads using \texttt{kron} and \texttt{urand} inputs;
(ii) GAP workloads using other inputs.

% \begin{itemize}
% 	\item GAP workloads using \texttt{kron} \& \texttt{urand} inputs;
% 	\item GAP workloads using other inputs.
% \end{itemize}

As figure \ref{fig:reuse_distances_distribution} shows,
workloads using \texttt{kron} and \texttt{urand} inputs tend to stress more the LLC.
As stated by Beamer \emph{et al.} in the specification of the benchmark, these
inputs present a very high memory footprint compared to the other
inputs used in this work and, moreover,
their graphs have a very distinct topology. \texttt{Urand}
represents a worst-case as every vertex has an equal probability of
being a neighbour of every other vertex. Thus, the graph processing
kernel in charge of traversing these inputs has to
request memory blocks very distant from another,
which eventually causes poor reuse.

As a result, we see that LLC replacement policies are impractical
solutions in this context as these workloads are biased towards a cache-averse behavior, all accesses
being cache-averse. Although replacement policies cannot improve
performance for these workloads, an excellent way to
cope with these workloads would be to increase the capacity of the cache
substantially or to incorporate a prefetcher that can predict the access patterns to the memory blocks
and bring them to the cache before they are accessed.

% \begin{figure}[h]
% 	\includegraphics[width=\columnwidth]{results/plots/gapbs_augmented_llc_mpk_reduction.pdf}
% 	\vspace{-0.5cm}
% 	\caption{LLC MPKI on GAP workloads with an augmented LLC capacity from \SI{2}{\mebi\byte} to \SI{16}{\mebi\byte}.}
%   \vspace{-0.5cm}
% 	\label{fig:llc_mpki_gapbs_increased_llc_capacity}
% \end{figure}

\begin{figure}[t]
  \centering
  \subfloat{
    \includegraphics[width=\columnwidth]{results/plots/gapbs_augmented_llc_mpk_reduction.pdf}
    \label{fig:llc_mpki_gapbs_increased_llc_capacity}
  }

  \vspace{-0.5cm}

  \subfloat{
    \includegraphics[width=\columnwidth]{results/plots/gapbs_augmented_llc_speedup.pdf}
    \label{fig:speedup_gapbs_increased_llc_capacity}
  }
  \vspace{-0.25cm}
  \caption{LLC MPKI reduction and speedup of a \SI{16}{\mebi\byte} LLC over the baseline \SI{2}{\mebi\byte} LLC.}
  \label{fig:gapbs_increased_llc_capacity}
  \vspace{-0.25cm}
\end{figure}

% Results in figure \ref{fig:llc_mpki_gapbs_increased_llc_capacity} show a clear benefit from the increased capacity of the LLC. It turns out the average MPKI reduction leveraged by the augmented capacity of the LLC.
To demonstrate the need for increased cache capacity, we increase the size of the LLC
%modify the modelled architecture and specifically the on-chip LLC, its size going
from \SI{2}{\mebi\byte} to \SI{16}{\mebi\byte} and present the results
in Figure~\ref{fig:gapbs_increased_llc_capacity}.
Results show a clear benefit from the increased capacity of the LLC.
The average MPKI over the whole set of GAP benchmarks
drops by \SI{18}{\percent}, which delivers a geometric mean speedup of \SI{6.1}{\percent}
compared with a \SI{2}{\mebi\byte} LLC with an LRU replacement policy. However,
some workloads show reduced MPKI while suffering from
an IPC slow-down. The reason for this this phenomenon are the DRAM
latencies. While increasing the LLC capacity, these workloads have similar
statistics in terms of cache accesses and miss rates, but the miss rate
in the DRAM row buffers increases. We interpret this behavior as a
symptom of workloads with extremely poor temporal and spatial
locality.

This clearly shows the need for improved memory
allocation policies when it comes to graph processing algorithms. To
illustrate this example, we focus on \texttt{pr.road}, as Page Rank is an
algorithm that should show better locality than the others.
When computing the score of an edge of the graph, the algorithm only
visits neighbouring edges to that edge. Thus, the only way for the DRAM
to exhibit higher row buffer miss rate is that neighbouring edges in
graphs are stored in very distant places in main memory. This showcases the
need for a memory allocation policy that takes into account the topology
of the graph.
Another way to improve the performance of these graph processing workloads
would be to incorporate a dedicated on-chip storage structure
able to serve, at a low cost, not only the required edges but also their neighbours.

To wrap up this discussion about GAP benchmark suite,
the presented results show that there is still a good amount of
work to be done on the
algorithmic and software side of these workloads to make them
cache-friendly.
%in the sense that they demonstrate no such thing as locality.
In addition, the reported characteristics of this benchmarks
are hard to exploit by traditional cache hierarchies
and cache replacement policies,
so different solutions should be explored on the hardware side
to improve the performance of these benchmarks.
