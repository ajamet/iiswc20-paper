\section{Motivation}
\label{sec:motivation}

% You might want to present the fact that when it comes to workloads, overusing some of them and not allowing for diversity might lead to an overfitting in the techniques. For instance we can think about LLC replacement policies not able to generalize to new kind of workloads because they were trained so much to fit only a few of them.

% To highlight the need for new benchmarks in the context of the development of new cache replacement policies for LLCs we proceed in two steps: first, we start by a high-level description of the current trends in big data and HPC workloads. Second, we provide quantitative analysis to build intuition on why the current state-of-the-art techniques need to take into account a broader set of workloads in the process of their constructions. This analysis relies on figures obtained through the simulation methodology detailed in \ref{sec:methodology}.

To highlight the need for new benchmarks in the context of the development of new cache replacement policies for LLCs, we provide a quantitative analysis to build intuition on why the current state-of-the-art techniques need to take into account a broader set of workloads in the process of their constructions. This analysis relies on results obtained using the simulation methodology detailed in \ref{sec:methodology}.

% \todo{Update the LLC MPKI numbers.}

% Here is the quantitative analysis.
Figure \ref{fig:llc_mpki_suites_split} shows the average LLC MPKI for each of the benchmark suites described in Section~\ref{sec:workloads} using the baseline LRU replacement policy. In both Figures \ref{fig:llc_mpki_suites_split} and \ref{fig:speedup_suites_split} we only consider the cache-intensive benchmarks of these benchamrks suites, namely the ones which present a LLC MPKI over \num{1.0} with the baseline LRU replacement policy. The GAP benchmark suite and all the different runs of XSBench, with respective LLC MPKI of \num{78.29} and \num{36.62}, provide a significantly higher LLC MPKI than what is provided by the SPEC CPU benchmarks. The industrial Qualcomm workloads and the SPEC CPU workloads, with respective LLC MPKI of \num{10.63} and \num{15.76}, do not show such a high impact on the LLC.

These results demonstrate that big data graph processing workloads like the ones modeled in the GAP benchmark suite highly stress the cache hierarchy, and particularly the LLC, much more than well-known workloads such as SPEC CPU 2006 and SPEC CPU 2017 suites do. This is due to the nature of these workloads, where moving from edge to edge in a graph structure leads to extremely unpredictable and sparse access patterns\cite{yu_imp_2015,zhang_making_2017}. Also, the memory footprint of the inputs is an important factor, as jumping from edges to edges in an extensively large graph exhibits very low spatial and temporal locality, which are two key concepts in the design of cache replacement policies.

% Hiding the figure as it does not carry much information.
% \begin{figure}[h]
%   \includegraphics[width=\columnwidth]{motivation/plots/avg_mpki_split_suites.pdf}
%   \vspace{-0.5cm}
%   \caption{Average LLC MPKI of the benchmark suites.}
%   \label{fig:llc_mpki_suites_split}
% \end{figure}

Figure \ref{fig:speedup_suites_split} shows the speed-up of the state-of-the-art cache replacement policies presented in Section~\ref{background} over the baseline LRU policy for the different benchmark suites presented in Section~\ref{sec:workloads}. Each bar represents a single replacement policy, and each bar group stands for a benchmark suite.
%When considering all the benchmark available, there is no clear choice about the best replacement policy.
Results show that the different policies can catch a different kind of access patterns and are beneficial for different kind of workloads.

For the SPEC benchmarks, the plot shows that every single replacement policy is consistently delivering improvements over the baseline LRU and the previously proposed replacement policies in the literature, incrementally improving the performance of these benchmarks.

% Notably, the case of \textit{XSBench} when MPPPB is used as a replacement policy in the LLC. It turns out, this speed-down only comes from the runs using the \textit{unionized} grid type while Glider is actually standing out, providing the best speed-up.

% One can also see that replacement poclies based on Reuse Distance Prediction (\textit{c.f.}: \ref{subsec:reuse_distance_prediction}) are consistent in their improvement over the baseline LRU while more complex policies such as SHiP, Hawkeye, Glider, MPPPB have more difficulties in generalizing to the studied benchmark suites. The main resason behind this observation is that all these replacement policies rely either on assumptions about the access patterns (\textit{e.g.}: ShiP and Hawkeye) or on a training phase over a set of workloads (\textit{e.g.}: Glider and MPPPB). On the on hand, SHiP and Hawkeye leverage on the observation that the access patterns to the LLC can be easily learned through the PCs used to trigger accesses to the memory. On the other hand, both Glider and MPPPB rely on a learning algorithm that learns the access patterns of a couple of workloads and eventually provide correlating features such as the \textit{i}-th PC of the history or some bits of the physical address of the accessed block.

The replacement policies based on Reuse Distance Prediction (SRRIP and DRRIP) are consistent in their improvement over the baseline LRU, while more complex policies such as SHiP, Hawkeye, Glider and MPPPB have more difficulties generalizing to all the benchmark suites. The main reason behind this observation is that all these replacement policies rely either on assumptions about the access patterns (\textit{e.g.}, SHiP and Hawkeye) or on a training phase over a set of workloads (\textit{e.g.}, Glider and MPPPB). On the one hand, SHiP and Hawkeye use the observation that they can accurately learn the access patterns to the LLC using the PCs that triggered the memory accesses as a classification feature. On the other hand, both Glider and MPPPB rely on a learning algorithm that learns the access patterns of a couple of workloads and provide correlating features such as the $i$-th PC of the history or some bits of the physical address of the accessed block.
Thus, although these state-of-the-art replacement policies deliver some performance improvements, they suffer from a structural bias that prevents them from generalizing to unexplored benchmarks in an optimal way.

% \begin{figure}[h]
% 	\includegraphics[width=\columnwidth]{motivation/plots/speedups_split_suites.pdf}
%   \vspace{-0.5cm}
% 	\caption{Speed-up over LRU of state-of-the-art LLC replacement policies for the different benchmark suites.}
% 	\label{fig:speedup_suites_split}
% \end{figure}

\begin{figure}[h]
	\centering
	\subfloat[Average LLC MPKI of the different benchmark suites using the LRU replacement policy.]{
		\includegraphics[width=\columnwidth]{motivation/plots/avg_mpki_split_suites.pdf}
		\label{fig:llc_mpki_suites_split}
	}

	\vspace{-0.25cm}

	\subfloat[Geometric mean speed-up over LRU of state-of-the-art LLC replacement policies for the different benchmark suites.]{
		\includegraphics[width=\columnwidth]{motivation/plots/speedups_split_suites.pdf}
		\label{fig:speedup_suites_split}
	}
	\caption{LLC MPKI using the LRU replacement policy and performance improvement of state-of-the-art LLC replacement policies for the different benchmark suites.}
	\vspace{-0.5cm}
\end{figure}

% As a whole, it appears that the benchmarks that were not used so far in research to develop and evaluate replacement policies show very distinct behaviour and react differently to the state of the art replacement policies. On the one hand, it suggests that researchers actually did a great job understanding SPEC workload's behaviour and building novel replacement policies to cope with the memory-wall by cleverly hiding latencies. On the other hand, it suggests that current state of the art techniques are biased towards a certain kind of workloads and behaviour and are not able to properly deal with common HPC workloads.

The main conclusions arising from our analysis are:

\begin{IEEEenumerate}
  \item The commonly used SPEC CPU 2006 and SPEC CPU 2017 suites no more represent a challenge for computer architects, as they are well studied and there are plenty of ingenious mechanisms that cope with their behavior.
  \item The current state-of-the-art LLC replacement policies do not generalize well to new benchmarks.
  \item Emerging big data and HPC workloads do represent a challenge for computer architects, as they stress more the cache hierarchy than traditional workloads. Nevertheless, they reveal the need to take into account their behavior in the design of forthcoming CPUs.
\end{IEEEenumerate}
