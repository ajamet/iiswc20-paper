\section{Design proposals}
\label{sec:design_proposal}

Along with the state-of-the-art cache replacement policies presented in
Section~\ref{background}, we introduce MS-MPPPB
and DS-MPPPB, two new LLC replacement policies derived from the original MPPPB.
These two policies try, in distinct manners, to adapt themselves to the
behavior of the emerging big data and HPC workloads.
%highly irregular workloads that we introduced in \ref{sec:workloads}.

% Although these designs do not provide a substantial improvement over the
% current state-of-the-art - as mentioned in
% \ref{subsec:performance} -, we
% argue that they could be promising research lines for further work on
% this topic.

% \todo{The hardware complexity should go at the end after explaining the actual designs.}

% \todo{Update table \ref{tab:reuse_distance_distribution_parameters} accordingly.}

\subsection{Multi-Sampler Multiperspective}
\label{subsec:multi_sampler_mpppb}

With MPPPB, Jiménez and Teran provide a replacement policy based on a reuse
predictor, which ultimately relies on a hashed perceptron table.

% Our first proposed design, named MS-MPPPB, is based on the idea that having
% not just one but many hashed perceptron tables can yield higher prediction accuracy
% and thus, improved performance.  To perform the selection of the best behaving
% perceptron, we use a two-rounds decision tree\cite{khan_insertion_2010} that
% duels each of the four available perceptron tables and selects the one that
% minimizes misses for the cache. To do so, we apply each perceptron to a small
% portion of the cache (32 sets each in our experimentations). For the first
% dueling round, we maintain two counters that monitor which of the four
% policies causes more cache misses. The selection mechanism applies the two
% winning policy of the first dueling round to two other small portions of the
% cache, and they duel for a second round. The policy selected at the end of
% this final round ultimately applies to the rest of the cache.

Our first proposed design, named MS-MPPPB, is based on the idea that having
not just one but many hashed perceptron tables can yield higher prediction accuracy
and improved performance by dynamically choosing one of the perceptrons to trigger predictions.
% To perform the selection of the best behaving
% perceptron, we use a two-rounds decision tree\cite{khan_insertion_2010} that
% duels each of the four available perceptron tables and selects the one that
% minimizes misses for the cache.
To perform the selection of the best behaving perceptron that will eventually trigger predictions,
all perceptrons are competing against each other though a two-rounds decision tree scheme~\cite{khan_insertion_2010}
that duels each of the four available perceptrons and selects the one that minimizes misses in the LLC.
Although not used to produce a prediction, the three perceptrons left unused are concurrently updated following the process
described by Jiménez and Teran in \textit{Multiperspective reuse prediction}~\cite{jimenez_multiperspective_2017}.

The additional hardware budget required for this proposal is rather high, as a
naive implementation would lead to the instantiation of 4 individual samplers
along with the 4 perceptron tables bound to them. Each block of the sampler
holds an indices trace of the last accessed elements of the prediction tables,
which requires a maximum of 128 bits. For each block, the sampler holds a
16-bit partial tag along with a 5-bit LRU state and a 9-bits confidence value.
The sampler takes the form of a cache with 80 sets and 18
ways.

\subsection{Multiperspective with Dynamic Features Selector}
\label{subsec:features_set_dynamic_selector_mpppb}

In the second design proposal, named DS-MPPPB, we use an additional concept
along with the already existing idea of weights. Our new concept,
\textit{Coefficients}, revisits the conception of a hashed
perceptron~\cite{tarjan_ahead_nodate} by introducing the weights used in the
mathematical definition of a perceptron~\cite{widrow_30_1990}.

We thus differentiate two key concepts. The \textit{weights} are the actual
values contained in the prediction tables of a hashed perceptron. These values
are meant to reflect the learned reuse distance based on the observation of
past events. These events can be the occurrence of a specific PC, a physical
address or any other source of information used as
feature\cite{khan_sampling_2010,lai_dead-block_nodate,liu_cache_2008}.
The \textit{coefficients} are
confidence counters that reflect how accurate is the prediction table bound to
a specific confidence counter.

% \begin{equation}
%   pred = \sum_{i = 0}^{n - 1} \frac{conf_i}{conf_{max,i}} t_i\left[f_i(x)\right]
% \end{equation}


% In an attempt to select the n-best behaving features among a total of m-features.
%
% The additional hardware complexity is fairly low have we only a single
% \(m\) bit counter per prediction table, \emph{e.g.}, in the evaluation
% performed in section
% \ref{sec:results_discussion},
% we used 10-bits saturating counters which eventually only 20 bytes of
% additional hardware budget, around 0.7\% of the hardware complexity of
% the original MPPPB.

The original code of MPPPB is shipped with not just one set of
features but four, which adds up to a total of 64 features. Each of these sets
of features was developed following the hill-climbing methodology described in
\textit{Multiperspective Reuse Prediction}\cite{jimenez_multiperspective_2017}
and is designed to fit each of the possible configurations of the CRC2
contest.

We gather all the features in a single set and build a perceptron predictor
using them all.  Although we now have a set of 64 features, we want to select
only the 16 best behaving ones among the 64 available. To do so, for each
prediction triggered by the replacement policy, the predictor searches for the
16 features with the highest confidence values and uses them to build a
prediction, and the other features are left unused for that prediction.
However, confidence values of all features are updated following algorithm
\ref{alg:confidence_values_update_algorithm}.

\begin{algorithm}
  \caption{Updating confidence values of prediction tables}
  \label{alg:confidence_values_update_algorithm}

  \begin{algorithmic}
    \STATE $hit \leftarrow \FALSE$
    \STATE $truth \leftarrow 0_{\mathbb{B}^n}$ \COMMENT{A n-vector of falses.}
    \STATE $pred \in \llbracket -32;31 \rrbracket^{n}$ \COMMENT{A vector of individual predictions.}
    \IF{Accessed block hits in the sampler}
      \STATE $hit \leftarrow \FALSE$
    \ELSE
      \STATE $hit \leftarrow \TRUE$
    \ENDIF

    \FORALL{$i$ such that $0\leq i\leq n-1$}
      \STATE $truth\left[i\right] \leftarrow \left(\left(pred\left[i\right] < 0\right) = hit\right)$
      \IF{$truth\left[i\right] = \TRUE$}
        \STATE $conf_{i} \leftarrow \max(conf_{i} + 1,conf_{max,i})$
      \ELSE
        \STATE $conf_{i} \leftarrow \min(conf_{i} - 1,0)$
      \ENDIF
    \ENDFOR
  \end{algorithmic}
\end{algorithm}

% For clarity, we include a brief summary of the notations we use. \(\mathbb{F}\)
% denotes the set of features. For conciseness \(n = Card(\mathbb{F})\) and $m \in \mathbb{N}, m \leq n$ the size of
% the subset of features we use for prediction.
% We denote the confidence counter of a given feature as
% \(conf(f_{i}) = conf_{i}\) along with the upper bound of the confidence
% counters \(conf_{max,i}\) such as
% \(\forall i \in \llbracket 0;n - 1 \rrbracket,0 \leq conf_{i} < conf_{max,i}\). We denote as
% \(t_{i}\) the prediction table associated with the feature
% \(f_{i} \in \mathbb{F}\). Finally, we have \(f_{i}(x)\) the index of the
% associated prediction computed out of the feature input.

For clarity, we include a summary of the notations we use. \(\mathbb{F}\)
denotes the set of features, $n$ is the total number of features and $m$ the number of features
we include in the prediction value.
We denote the confidence counter of the $i$-th feature as
\(conf(f_{i}) = conf_{i}\) along with the upper bound of the confidence
counters \(conf_{max,i}\). We denote as
\(t_{i}\) the prediction table associated with feature \(f_{i}\).

% Finally, we have \(f_{i}(x)\) the index of the
% associated prediction table computed out of the feature input.

% We thus define $\breve{\mathbb{F}}$ the set of features' set of size $m$ and its most confident element $\breve{\mathbb{F}}_{max}$ as:

We thus define $\breve{\mathbb{F}}$, the set of all possible arrangements of unique $m$ features taken out of $\mathbb{F}$ and
$\breve{\mathbb{F}}_{max}$ the element of $\breve{\mathbb{F}}$ that maximizes the sum of confidence counters.
Finally, we compute the prediction value by summing the weights taken out of the tables of the elements of
$\breve{\mathbb{F}}_{max}$.

% \begin{eqnarray}
%   \label{eq:set_of_features_sets_m_size}
%   \breve{\mathbb{F}} =       & \{ \tilde{\mathbb{F}} \in \mathcal{P}(\mathbb{F}) | Card(\tilde{\mathbb{F}}) = m \} \\
%   % \breve{\mathbb{F}}_{max} = & max_{\breve{\mathbb{F}}_i \in \breve{\mathbb{F}}} \sum_{\breve{f}_i \in \breve{\mathbb{F}}} conf(\breve{f}_i) \\
%   \breve{\mathbb{F}}_{max} = & max_{\breve{\mathbb{F}}_i \in \breve{\mathbb{F}}} \sum_{\substack{
%     \breve{f}_i \in \breve{\mathbb{F}} \\
%     0 \leq i < m
%   }} conf(\breve{f}_i)
% \end{eqnarray}

% \begin{equation}
%   \label{eq:set_of_features_sets_m_size}
%   % \breve{\mathbb{F}} =       & \{ \tilde{\mathbb{F}} \in \mathcal{P}(\mathbb{F}) | Card(\tilde{\mathbb{F}}) = m \} \\
%   % \breve{\mathbb{F}}_{max} = & max_{\breve{\mathbb{F}}_i \in \breve{\mathbb{F}}} \sum_{\breve{f}_i \in \breve{\mathbb{F}}} conf(\breve{f}_i) \\
%   \breve{\mathbb{F}}_{max} = max_{\breve{\mathbb{F}}_i \in \breve{\mathbb{F}}} \sum_{\substack{
%     \breve{f}_i \in \breve{\mathbb{F}}
%     0 \leq i < m
%   }} conf(\breve{f}_i)
% \end{equation}

% \begin{equation}
%   \label{eq:prediction_value}
%   pred = \sum_{\substack{
%     \breve{f}_{i,max} \in \breve{\mathbb{F}}_{max} \\
%     0 \leq i < m
%   }} \breve{t}_{i,max}\left[\breve{f}_{i,max}(x)\right]
% \end{equation}

Table \ref{tab:designs_hardware_budget} summarizes the hardware budget of each design proposal described in this section.
Along with the total hardware budget required for each proposal, we also provide the budget required by each component, namely:
the replacement states (here we use MDPP, a modified Tree-based PLRU\cite{jimenez_insertion_2013,teran_minimal_2016} policy that uses a custom transition vector to determine to which
position an accessed block should be moved to), the sampler(s) and perceptron(s).

\input{designs/table_hardware_budget.tex}

% \todo{Describe in more details the process to obtain these sets of features, what they were made for and also
% print them.}
