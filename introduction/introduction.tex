\section{Introduction}

	The vast disparity between main memory and CPU speed has led to hierarchical caching system in modern CPUs. The goal of the cache hierarchy is to keep data on-chip, close to the cores that are accessing it, thus avoiding hitting the memory wall~\cite{wulf_hitting_1995}.
%which is no more than the vast disparity between the speed of modern CPUs and their memory sub-systems.
Although computer architects highlighted the need for multiple levels to the cache hierarchy, the Last Level Cache (LLC) suffers from a high latency compared to the other cache levels. In addition, the LLC suffers from poor temporal and spatial locality in the access sequence as some accesses are filtered by the upper levels of the hierarchy. This phenomenon is exacerbated when considering emerging workloads such as big data or graph-processing workloads displaying highly irregular behavior. Thus, emerging workloads require more sophisticated cache replacement policies that can cope with a broader set of workloads than the traditional ones. %, and able to distinguish which block should remain in the cache and which should be effectively thrown away.

State-of-the-art LLC replacement policies such as MPPPB~\cite{jimenez_multiperspective_2017}, Glider~\cite{shi_applying_2019}, Hawkeye~\cite{jain_back_2016}, SHiP~\cite{wu_ship:_2011}, DRRIP, and SRRIP~\cite{jaleel_high_2010} show significant improvement when challenged by SPEC CPU 2006~\cite{jaleel_memory_2016} and SPEC CPU 2017 workloads. However, when facing workloads representative of another part of the spectrum of the HPC applications, these policies fail at delivering significant improvement over the baseline LRU policy. Such workloads with highly irregular behavior prevent the LLC replacement policies mentioned above from capturing the access patterns and, therefore, producing meaningful predictions and decisions. To address this issue, we argue that future work on LLC replacement policies should consider a more extensive set of workloads such as the one we study in this paper, which is composed of the following benchmark suites:

\begin{IEEEitemize}
  \item the GAP benchmark suite\cite{beamer_gap_2017}.
  \item the XSBench benchmark suite\cite{tramm_xsbench_2014}.
  \item Qualcomm workloads for the CVP1\cite{noauthor_championship_nodate} championship.
\end{IEEEitemize}

% Presenting the new LLC replacement policies that we propose through this paper.
This paper also proposes two MPPPB variants that increase its benefits. First, we propose \textit{Multi-Sampler Multiperspective (MS-MPPPB)}, a variant of MPPPB that uses four samplers and perceptron structures. MS-MPPPB adapts its replacement policy to the workload in a phase-wise manner, selecting the sampler that provides the best predictions out of the four available and using the most accurate sampler to make predictions and drive placement, promotion and bypass decisions in the LLC.
Second, this paper proposes \textit{Multiperspective with Dynamic Features Selector (DS-MPPPB)}, another variant of MPPPB that is also able to adapt its behavior to the execution phases of the workloads by dynamically selecting the most accurate subset of 16 features from a bigger pool of 64 features.

%\subsection{Contributions}

This paper makes the following contributions:

\begin{IEEEenumerate}
  \item It evaluates state-of-the-art LLC replacement policies over a broader set of benchmark suites than usually considered in the literature. The selected benchmark suites better represent current and emerging big data and scientific workloads on HPC systems. This paper considers the SPEC CPU 2006 and the SPEC CPU 2017 suites, a large set of workloads provided by Qualcomm for the CVP1 championship, the GAP benchmark suite, and the XSBench. This paper also takes the opportunity to build knowledge on these workloads and analyzes their behavior and impact on the LLC and the memory hierarchy, thus paving the way for further work.
	\item We present MS-MPPPB and DS-MPPPB, two novel LLC replacement policies derived from MPPPB. The main idea behind both schemes is to improve the accuracy of the predictions by dynamically selecting the most accurate features for each phase of the running workload. On a set of 50 cache intensive benchmarks, these new designs respectively yield a geometric mean speed-up of \SI{8.3}{\percent} and \SI{8.0}{\percent} over LRU, and outperform all the state-of-the-art approaches. %for a large collection of benchmarks.
  % \item Through a couple of custom cache replacement policies based on MPPPB, we present probable future research lines on LLC replacement policies such as the dynamic recognition of correlating sets of features in an effort to adapt the behaviour of replacement policies to not yet studied benchmarks. On a set of 50 single-thread benchmarks, this scheme yields a geometric mean speed-up of 8.3\% over LRU, compared with 7.0\% for MPPPB, 5.0\% for Glider and 4.8\% for Hawkeye.
  % \item It provides a comprehensive analysis of the behaviour of these workloads in the LLC and thus maps out the route to potential architectural and software optimizations that could lead to increased performance.
\end{IEEEenumerate}

% \todo{The rest of this paper is organized as follows: ...}

The rest of this paper is organized as follows:
%Section \ref{sec:workloads} describes the different workloads studied in
%the paper. Section \ref{background} presents state-of-the-art replacement policies evaluated in this work.
Sections \ref{sec:workloads} and \ref{background} describe the workloads and the state-of-the-art replacement policies evaluated in this paper, respectively.
Section \ref{sec:motivation} motivates the need for additional benchmarks in the evaluation of LLC replacement policies.
Section \ref{sec:design_proposal} proposes MS-MPPPB and DS-MPPPB, two designs derived from
MPPPB that achieve higher accuracy on the studied benchmarks.
Section \ref{sec:methodology} defines the evaluation methodology.
Section \ref{sec:results_discussion} presents the results of our experiments and comment on them.
Finally, Section~\ref{sec:conclusions} remarks the main conclusions of this work.
