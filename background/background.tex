\section{Cache Replacement Policies}\label{background}

While developing new cache replacement algorithms for LLCs,
one needs to evaluate the policy against a set of workloads that show
the behavior of interest. This section reviews the most relevant
cache replacement algorithms designed for LLCs.
%along with some benchmarks suite available to researchers.
As this work studies the impact of emerging workloads on the LLC, we present
the state-of-the-art replacement policies developed for this specific cache
level. The cache replacement problem is slightly more complex in the context
of the LLC than in the context of L1 and L2 caches.  Although the underlying
idea remains the same, the LLC suffers from poor locality as the
upper-level caches filter accesses and leave only a cluttered sequence to the
LLC. To cope with this particular replacement problem, researchers have come up
with more and more sophisticated design ideas to leverage higher prediction
accuracy and performance.

The next subsections present the most relevant state-of-the-art work on LLC replacement policies.

% \todo{We should add ShiP in the next subsections.}

\subsection{Reuse Distance Prediction}\label{reuse-distance-prediction}

As reuse distance is a crucial concept when it comes to cache replacement,
recent works focus on proposing new techniques to build run-time
approximations of the distance to the next reuse of a cache block.
Re-reference Interval Prediction (RRIP) and all its derivatives are efficient
yet light-weight implementations of reuse distance prediction.

The main idea behind RRIP is the classification of blocks into re-reference
classes. In their work, Jaleel {\em et al.} propose three versions of the RRIP
replacement policies\cite{jaleel_high_2010,qureshi_adaptive_2007}: SRRIP,
BRRIP, and DRRIP. The former, scan-resistant, is limited to always inserting
new coming blocks in a fixed class. In contrast, BRRIP provides more flexibility by
frequently inserting blocks in the distance re-reference class
and infrequently inserting blocks in the long re-reference class.
Finally, DRRIP leverages Set-Dueling to determine
which of SRRIP and BRRIP is best suited for a given workload or program phase,
making it both scan and thrash resistant.

\subsection{Signature-based Hit Predictor}

Building on the reuse distance
prediction~\cite{jaleel_high_2010,qureshi_adaptive_2007} framework built by
Jaleel {\em et al.} and program-counter based dead block
prediction~\cite{khan_sampling_2010}, Wu {\em et al.}\cite{wu_ship:_2011} proposed a LLC
replacement policy design that uses a program-counter based signature as a
feature.

As stated while describing reuse distance prediction mechanism in
Section~\ref{reuse-distance-prediction}, SRRIP learns the re-reference
intervals of the living cache blocks relatively to one another. The primary
feature of \textit{Signature-based Hit Predictor (SHiP)}~\cite{wu_ship:_2011}
is that, not only it allows the SRRIP policy to learn the relative
re-reference intervals, but it also tries to learn the likelihood of cache
blocks to experience hits through a feature. The intuition being that cache
blocks with the same signature behave comparably. In order to learn the
likelihood of a cache block to experience further hits, SHiP maintains a
prediction table with an entry per signature. When a signature gets hit, the
associated saturating counter is incremented. Conversely, when a signature
misses, the associated counter is decremented.

With the prediction values thus learned, SHiP modifies SRRIP policy for
insertion by inserting new coming cache blocks in the distant re-reference
interval if the prediction associated with the signature of that cache blocks
is zero.  A zero in the prediction table gives a strong hint that the
associated signature belongs to the distance re-reference interval.

\subsection{Multiperspective Reuse Prediction}
\label{multiperspective-reuse-prediction}

The \textit{Multiperpsective Reuse
Prediction}\cite{jimenez_multiperspective_2017} cache replacement algorithm
(hereafter MPPPB) leverages perceptron learning for reuse prediction and
drives placement, promotion and bypass decisions. This replacement policy
extends the idea of features developed in previous
work~\cite{khan_sampling_2010,wu_ship:_2011,teran_perceptron_2016} to achieve
higher accuracy. It is essentially made of two components, a sampler and a
perceptron predictor.  The sampler, based on observations of block evictions
relatively to its features associativity, is responsible for triggering
learning signals to the perceptron. The perceptron, based on the learning
signals triggered by the sampler, updates its prediction tables.

MPPPB relies on the idea of correlating reuse prediction of a cache block with
a large number of features that ranges from PCs to characterizing bursty
access patterns.
%LLC sets and improves a previously proposed design\cite{teran_perceptron_2016}.
In this context, a feature can be defined
as a hash function applied to cache block characteristics such as the PC or
the physical address.  When a prediction request occurs, the perceptron
selects weights out of its prediction tables using hashes of multiple
features.  Each feature is hashed to index its prediction table, and the
weights obtained are gathered in a single prediction value by a simple addition
and compared to a set of thresholds to drive actions such as bypass, promotion
and placement.

Perceptron learning is used to update the weights of the
prediction tables through the learning algorithm.
At the time a sampled block is reused or evicted, the
perceptron updates the weights of the prediction tables associated with
the last access to this block, according to the perceptron learning rule.
For instance, if a block hits in the sampler while having its LRU stack
position lower than the associativity of a feature, it is trained
positively for that feature. Conversely, if a block gets demoted beyond
the associativity of a given feature, it is trained negatively for that
feature.

With this work, Jiménez and Teran demonstrated the usefulness and impact
of combining multiple features. Among the correlating features, the
sequence of PCs leading to the usage of a block is one; however, the
sequence of PCs is highly filtered by the other levels of the cache hierarchy,
making it inaccurate for predictions. The introduced additional
features such as bits extracted from the memory address help mitigating
the inaccuracy of a filtered PC sequence. MPPPB relies on this idea of
combining multiple features while significantly augmenting the set of
available features.

\subsection{Optimal Replacement
Approximation}\label{optimal-replacement-approximation}

The Hawkeye\cite{jain_back_2016} replacement policy marked the birth of a new
class of cache replacement algorithms aiming at approximating, in a relatively
affordable way, optimal but unimplementable algorithms such
MIN~\cite{belady_study_1966}.

Hawkeye and its successor, Glider\cite{shi_applying_2019}, are primarily made of two major
building blocks: an optimal solution approximation component and a
predictor that learns from the former component. The predictor is
used to compute predictions and to trigger actions based on these predictions. The first
component provides a binary output about the cache block of interest:
needs to be cached or not. For this outcome, the predictor gets trained
for the associated PC as it is a PC-based predictor. When the
replacement policy requests a prediction to drive its decision making,
the predictor is indexed, and it uses its outcome to place blocks in the
matching RRIP categories, thus prioritizing eviction for blocks
classified as cache-averse. Conversely, blocks identified as
cache-friendly tend to stay in the immediate-reuse category.

Further work on the Hawkeye predictor provided it with a more complex
predictor infrastructure. That infrastructure, named
Glider, leveraged on the knowledge obtained through
the offline training of a machine learning model, yielding additional
performance improvements.
