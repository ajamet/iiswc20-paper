\section{Methodology}
\label{sec:methodology}

In this section we present the evaluation methodology used to report results
in Section~\ref{sec:results_discussion}. In particular, the next subsections
present the set of workloads used to evaluate the different LLC replacement
policies and our workload selection methodology, a description of the
simulation environment, and the evaluated replacement policies and their
configuration.

%\begin{IEEEenumerate}
%	\item a brief presentation of the evaluated replacement policies along with their configuration.
%	\item the set of workloads used to evaluate the different LLC
%	replacement policies and our workload selection methodology.
%	% \ref{sec:workloads} while reducing the amount of computation required.
%	\item A description of the experimental setup.
%\end{IEEEenumerate}

Overall, we follow the same evaluation methodology as the one used by Shi {\em
et al.}~\cite{shi_applying_2019} with the aim of building the fairest
comparison possible against state-of-the-art techniques.

\subsection{Workloads}
\label{subsec:workloads}

For the evaluation of the different LLC replacement policies we consider the
following sets of workloads:

\begin{IEEEitemize}
  \item Over 2000 Qualcomm workloads used for CVP1 contest.
  \item All SPEC CPU 2006 and CPU 2017 benchmarks.
  \item All workloads included in the GAP Benchmark Suite.
  \item All workloads included in the XSBench Suite.
\end{IEEEitemize}

From all these benchmarks we select the 50 most intensive workloads so that
our evaluation set of workloads is a blend of each suite designated above.  We
use the SimPoints~\cite{perelman_using_nodate} methodology to identify
intervals (hereafter \emph{SimPoints}) representative of at least
\SI{5}{\percent} of the SPEC, GAP and XSBench workloads. Each SimPoint is 1
billion instructions long and characterizes a different phase of these
workloads. Each SimPoint is executed for 200 million instructions in order to
warm-up the memory hierarchy, and then it is executed for an additional 1
billion instructions to report experimental results.

% Wat we want to evaluate in this paper is more the application behaviour so the methodology should reflect this orientation.
We only evaluate these workloads in a single-thread context. We deliberately
chose to restrict our evaluation to single-core as this work focuses on the
characterization of the access patterns of the selected workloads to the LLC.
The modeled architecture being composed of a shared LLC, modeling an
architecture using multiple cores we would not be able to properly measure
reuse distances as the different cores would be asking for distinct data in the same
cache, thus compromising our measurements. The results reported per benchmark
(for SPEC, GAP and XSBench) are the normalized weighted averages of the
results for individual SimPoints. In contrast, the Qualcomm workloads are
single-trace benchmarks that do not use such methodology.

%The SimPoint tool generates weights used to construct an approximation of the results of the whole benchmarks.

\subsection{Experimental setup}

Our evaluation considers ChampSim\cite{champsim_champsimchampsim_2020}, a
detailed trace-based simulator that models an out-of-order CPU along with its
cache hierarchy, prefethcing mechanisms and memory sub-system. Table~\ref{tab:cpu_config} provides a
detailed configuration of the modeled CPU and the memory hierarchy.

\input{methodology/table_simulation_parameters.tex}

\subsection{Replacement policies simulated}
\label{subsec:replacement_policies_simulated}

We evaluate the workloads described in Section~\ref{sec:workloads} against the
most relevant cache replacement policies proposed in the literature: SRRIP,
DRRIP, SHiP, MPPPB, Hawkeye and Glider, all detailed in
Section~\ref{background}.  Although there is a vast amount of work in reuse
prediction available in the literature
\cite{abella_iatac_2005,beckmann_maximizing_2017,duong_improving_2012,zhigang_hu_timekeeping_2002,jaleel_high_2010,jimenez_insertion_2013,khan_sampling_2010,kharbutli_counter-based_2008,teran_minimal_2016,teran_perceptron_2016},
the aforementioned replacement policies that have been selected for the
evaluation are the most recent and relevant approaches in the
state-of-the-art.  In addition, in the evaluation we also include the two new
replacement policies proposed in this paper, explained in
Section~\ref{sec:design_proposal}.  These two new techniques are derived from
MPPPB and leverage the usage of multiple perceptrons to achieve higher
accuracy. For MPPPB we used the code that is publicly available on the website
of the CRC2 contest~\cite{gratz_2nd_2017}.  For Glider we use code
graciously provided by the authors.

% Note that these implementations have been provided by the original authors of the corresponding works.
