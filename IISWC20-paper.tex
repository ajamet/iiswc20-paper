\documentclass[10pt,conference,twoside]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}\DeclareUnicodeCharacter{2212}{-}\DeclareUnicodeCharacter{FB01}{fi}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts,stmaryrd}
\usepackage[binary-units]{siunitx}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage[export]{adjustbox}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{pgf}
\usepackage{layouts}
\usepackage{flushend}
\usepackage{balance}

% Declaring normalized multiples for data size.
\DeclareBinaryPrefix\kibi{Ki}{10}
\DeclareBinaryPrefix\mebi{Mi}{20}
\DeclareBinaryPrefix\gibi{Gi}{30}

\ifCLASSOPTIONcompsoc
	\usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf]{subfig}
\else
	\usepackage[caption=false,font=footnotesize]{subfig}
\fi

% A couple of helpers stolen from Georgios.
\newcommand{\toask}[1]{\textcolor{red}{(Q): #1}}
\newcommand{\todo}[1]{\textcolor{red}{(ToDo): #1}}
\newcommand{\tocite}{\textcolor{blue}{[citation]}}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

% \printinunitsof{in}\prntlen{\textwidth}

% \title{Characterizing the impact of big data workloads on cache replacement policies}
\title{Characterizing the impact of last-level cache replacement policies on big-data workloads}

% Should remain commented for reviews.
% Specifying authors using the alternate format according the IEEEtran manual.
\author{
  % BSC affiliated authors.
  \IEEEauthorblockN{Alexandre Valentin Jamet\IEEEauthorrefmark{1}, Lluc Alvarez\IEEEauthorrefmark{1}\IEEEauthorrefmark{2}, Daniel A. Jiménez\IEEEauthorrefmark{3}, Marc Casas\IEEEauthorrefmark{1}}
  \IEEEauthorblockA{\IEEEauthorrefmark{1}Barcelona Supercomputing Center (BSC), \IEEEauthorrefmark{2}Universitat Politècnica de Catalunya (UPC), \IEEEauthorrefmark{3}Texas A\&M University\\
    \texttt{\{alexandre.jamet, lluc.alvarez, marc.casas\}@bsc.es, djimenez@acm.org}}
  % \and
  % % Texas A&M affiliated authors.
  % \IEEEauthorblockN{Daniel A. Jiménez\IEEEauthorrefmark{3}}
  % \IEEEauthorblockA{\IEEEauthorrefmark{3}Texas A\&M University\\
  %   \texttt{\href{mailto:djimenez@acm.org}{djminenez@acm.org}}}
}

% Allow to correctly generate the title page when switching to peerreview mode.
% To enable the peerreview mode just switch the document class option from conference to peerreview.
% \documentclass[10pt,peerreview,twoside]{IEEEtran}
\ifCLASSOPTIONpeerreview
	\IEEEpeerreviewmaketitle
\else
	\maketitle
\fi

\begin{abstract}
	The vast disparity between Last Level Cache (LLC) and memory latencies has motivated the need for efficient cache management policies. The computer architecture literature abounds with work on LLC replacement policy. Although these works greatly improve over the least-recently-used (LRU) policy, they tend to focus only on the SPEC CPU 2006 benchmark suite -- and more recently on the SPEC CPU 2017 benchmark suite -- for evaluation. However, these workloads are representative for only a subset of current High-Performance Computing (HPC) workloads.

	In this paper we evaluate the behavior of a mix of graph processing, scientific and industrial workloads (GAP, XSBench and Qualcomm) along with the well-known SPEC CPU 2006 and SPEC CPU 2017 workloads on state-of-the-art LLC replacement policies such as Multiperspective Reuse Prediction (MPPPB), Glider, Hawkeye, SHiP, DRRIP and SRRIP. Our evaluation reveals that, even though current state-of-the-art LLC replacement policies provide a significant performance improvement over LRU for both SPEC CPU 2006 and SPEC CPU 2017 workloads, those policies are hardly able to capture the access patterns and yield sensible improvement on current HPC and big data workloads due to their highly complex behavior.

	In addition, this paper introduces two new LLC replacement policies derived from MPPPB. The first proposed replacement policy, \textit{Multi-Sampler Multiperspective (MS-MPPPB)}, uses multiple samplers instead of a single one and dynamically selects the best-behaving sampler to drive reuse distance predictions. The second replacement policy presented in this paper, \textit{Multiperspective with Dynamic Features Selector (DS-MPPPB)}, selects the best behaving features among a set of 64 features to improve the accuracy of the predictions. On a large set of workloads that stress the LLC, MS-MPPPB achieves a geometric mean speed-up of 8.3\% over LRU, while DS-MPPPB outperforms LRU by a geometric mean speedup of 8.0\%. For big data and HPC workloads, the two proposed techniques present higher performance benefits than state-of-the-art approaches such as MPPPB, Glider and Hawkeye, which yield geometric mean speedups of 7.0\%, 5.0\% and 4.8\% over LRU, respectively.
\end{abstract}

\begin{IEEEkeywords}
cache management, big data, graph processing, workload evaluation, micro-architecture
\end{IEEEkeywords}

% Adding the different sections through a couple of input directives.
\input{introduction/introduction.tex}
\input{workloads/workloads.tex}
\input{background/background.tex}
\input{motivation/motivation.tex}
\input{designs/designs.tex}
\input{methodology/methodology.tex}
\input{results/results.tex}
\input{conclusion/conclusion.tex}

\section*{Acknowledgments}
\addcontentsline{toc}{section}{Acknowledgment}

This work has been partially supported by the European Union’s Horizon 2020 research and innovation program under the Mont-Blanc 2020 project (grant agreement 779877).
Lluc Alvarez has been partially supported by the Spanish Ministry of Economy, Industry and Competitiveness under the Juan de la Cierva Formacion fellowship number FJCI-2016-30984.
Marc Casas has been partially supported by the Spanish Ministry of Economy, Industry and Competitiveness under Ramon y Cajal fellowship number RYC-2017-23269.
This research was supported by National Science Foundation grant CCF-1912617, Semiconductor Research Corporation project 2936.001, and generous gifts from Intel Labs.

\newpage

% Here is all bibliography related-content.
\bibliographystyle{IEEEtranS}
\balance
\bibliography{references}
\end{document}
